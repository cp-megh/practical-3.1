package com.example.firstcomposeapplication.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.firstcomposeapplication.R
import com.example.firstcomposeapplication.fonts

@Composable
fun LoginView(navController: NavController) {

    val email = remember { mutableStateOf(TextFieldValue()) }
    val emailErrorState = remember { mutableStateOf(false) }
    val passwordErrorState = remember { mutableStateOf(false) }
    val password = remember { mutableStateOf(TextFieldValue()) }
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .background(Color(255, 255, 255))
            .fillMaxSize()
            .padding(all = 16.dp)
            .verticalScroll(scrollState),

        horizontalAlignment = Alignment.CenterHorizontally

    ) {
        Image(painter = painterResource(id = R.drawable.dog1),
            contentDescription = "Dog 1",
            modifier = Modifier
                .size(height = 200.dp, width = 200.dp)
                .padding(start = 0.dp, top = 50.dp, end = 0.dp, bottom = 0.dp)
                .clip(CircleShape))

        Text("Sign In",
            fontFamily = fonts,
            fontWeight = FontWeight.Bold,
            color = Color(255, 182, 26),
            fontSize = 40.sp,
            modifier = Modifier.padding(bottom = 30.dp))
        Spacer(modifier = Modifier.size(16.dp))

        OutlinedTextField(
            value = email.value,
            shape = RoundedCornerShape(50.dp),
            onValueChange = {
                if (emailErrorState.value) {
                    emailErrorState.value = false
                }
                email.value = it
            },
            isError = emailErrorState.value,
            modifier = Modifier.fillMaxWidth(),
            label = { Text("Email Id", fontFamily = fonts) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color(255, 182, 26),
                focusedLabelColor = Color(255, 182, 26)
            )
        )

        if (emailErrorState.value) {
            Text(text = "Required*", fontFamily = fonts, color = Color.Red)
        }
        Spacer(Modifier.size(16.dp))
        val passwordVisibility = remember { mutableStateOf(true) }

        OutlinedTextField(
            value = password.value,
            shape = RoundedCornerShape(50.dp),
            onValueChange = {
                if (passwordErrorState.value) {
                    passwordErrorState.value = false
                }
                password.value = it
            },
            isError = passwordErrorState.value,
            modifier = Modifier.fillMaxWidth(),
            label = { Text("Password", fontFamily = fonts) },
            trailingIcon = {
                IconButton(onClick = {
                    passwordVisibility.value = !passwordVisibility.value
                }) {
                    Icon(
                        imageVector = if (passwordVisibility.value) Icons.Default.VisibilityOff else Icons.Default.Visibility,
                        contentDescription = "visibility",
                        tint = Color.Red
                    )
                }
            },
            visualTransformation = if (passwordVisibility.value) PasswordVisualTransformation() else VisualTransformation.None,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = Color(255, 182, 26),
                focusedLabelColor = Color(255, 182, 26)
            ),
        )

        if (passwordErrorState.value) {
            Text(text = "Required*", fontFamily = fonts, color = Color.Red)
        }
        Spacer(Modifier.size(16.dp))

        Button(modifier = Modifier
            .padding(top = 10.dp)
            .size(120.dp, 50.dp),
            colors = ButtonDefaults.buttonColors(backgroundColor = Color(255, 182, 26)),
            shape = RoundedCornerShape(50.dp),
            onClick = {
                when {
                    email.value.text.isEmpty() -> {
                        emailErrorState.value = true
                    }
                    password.value.text.isEmpty() -> {
                        passwordErrorState.value = true
                    }
                    else -> {
                        passwordErrorState.value = false
                        emailErrorState.value = false
                        navController.navigate("home") {
                            popUpTo(navController.graph.startDestinationId)
                            launchSingleTop = true
                        }
                    }
                }
            }) {
            Text("Login", fontFamily = fonts, fontSize = 15.sp)
        }
        Spacer(modifier = Modifier.padding(top = 80.dp))
        TextButton(
            shape = RoundedCornerShape(50.dp),
            onClick = {
                navController.navigate("registration") {
                    popUpTo(navController.graph.startDestinationId)
                    launchSingleTop = true
                }

            }
        ) {
            Text(
                text = "Not Registered Yet? Click Here for Sign Up",
                fontFamily = fonts,
                fontSize = 15.sp,
            )
        }
        Spacer(modifier = Modifier.padding(top = 100.dp))
    }

}
