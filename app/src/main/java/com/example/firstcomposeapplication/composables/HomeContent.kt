package com.example.firstcomposeapplication.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.firstcomposeapplication.DogsListItem
import com.example.firstcomposeapplication.dataProvider.DogsDataProvider
import com.example.firstcomposeapplication.fonts
import com.example.firstcomposeapplication.R
import com.example.firstcomposeapplication.views.DogsChipsItems


@Composable
fun HomeContent(navController: NavController) {

    val dogsList = remember {
        DogsDataProvider.dogList
    }


    Column(
        modifier = Modifier
            .background(Color(0, 0, 0, 7))
    ) {

        Row(modifier = Modifier.fillMaxWidth()) {

            Column(modifier = Modifier.weight(2f)) {
                    Text(text = "Adopt Puppy",
                        fontFamily = fonts,
                        fontWeight = FontWeight.Bold,
                        fontSize = 35.sp,
                        modifier = Modifier.padding(20.dp))

                    Text(text = "your favourite soulmate pet",
                        fontFamily = fonts, fontWeight = FontWeight.Bold,
                        color = Color.DarkGray,
                        modifier = Modifier.padding(start = 20.dp, bottom = 20.dp))

            }

            Column(modifier = Modifier.weight(1f)) {
                Image(
                    painter = painterResource(id = R.drawable.dog4),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .padding(top = 25.dp,start = 30.dp, end = 30.dp)
                        .size(80.dp)
                        .align(Alignment.End)
                        .clip(CircleShape)
                )
            }


        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 10.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            val textState = remember { mutableStateOf(TextFieldValue("")) }
            SearchView(textState)
        }

        Row {
            LazyRow(contentPadding = PaddingValues(8.dp)) {
                items(
                    items = dogsList,
                    itemContent = {
                        DogsChipsItems(dogs = it)
                    }
                )
            }
        }

        Row {
            LazyColumn(contentPadding = PaddingValues(15.dp)) {
                items(
                    items = dogsList,
                    itemContent = {
                        DogsListItem(dogs = it)
                    })
            }
        }

    }

}

@Composable
fun SearchView(state: MutableState<TextFieldValue>) {
    TextField(
        value = state.value,
        label = { Text("Search your soulmate pet", fontFamily = fonts) },
        onValueChange = { value ->
            state.value = value
        },
        modifier = Modifier
            .fillMaxWidth()
            .padding(start=20.dp, end = 20.dp),
        textStyle = TextStyle(color = Color(0, 0, 0, 154), fontSize = 15.sp),
        trailingIcon = {
            Icon(
                Icons.Default.Search,
                contentDescription = "",
                modifier = Modifier
                    .padding(15.dp)
                    .size(24.dp)
            )
        },
        shape = RoundedCornerShape(corner = CornerSize(20.dp)),
        singleLine = true,
        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.DarkGray,
            cursorColor = Color.Black,
            trailingIconColor = Color(255, 152, 0, 255),
            backgroundColor = Color.White,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent
        )
    )
}
