package com.example.firstcomposeapplication.composables

import android.widget.Toast
import androidx.compose.foundation.*
import com.example.firstcomposeapplication.R
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.firstcomposeapplication.fonts

@Composable
fun RegistrationView(navController: NavController) {
    val context = LocalContext.current
    val email = remember { mutableStateOf(TextFieldValue()) }
    val password = remember { mutableStateOf(TextFieldValue()) }
    val confirmPassword = remember { mutableStateOf(TextFieldValue()) }
    val emailErrorState = remember { mutableStateOf(false) }
    val passwordErrorState = remember { mutableStateOf(false) }
    val confirmPasswordErrorState = remember { mutableStateOf(false) }
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
            .verticalScroll(scrollState)
            .background(Color(255, 255, 255)),
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        Image(painter = painterResource(id = R.drawable.dog4),
            contentDescription = "Dog 1",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .size(150.dp, 200.dp)
                .padding(top = 50.dp)
                .clip(CircleShape)
                .border(2.dp, color = colorResource(id = R.color.orange_700), CircleShape))


        Spacer(modifier = Modifier.size(16.dp))

        Text("Create Account",
            fontFamily = fonts,
            fontWeight = FontWeight.Bold,
            color = colorResource(id = R.color.orange_700),
            fontSize = 40.sp,
            modifier = Modifier.padding(bottom = 20.dp))
        Spacer(modifier = Modifier.size(16.dp))

        OutlinedTextField(
            value = email.value,
            shape = RoundedCornerShape(50.dp),
            onValueChange = {
                if (emailErrorState.value) {
                    emailErrorState.value = false
                }
                email.value = it
            },
            modifier = Modifier.fillMaxWidth(),
            isError = emailErrorState.value,
            label = { Text("Email Id", fontFamily = fonts) },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = colorResource(id = R.color.orange_700),
                focusedLabelColor = colorResource(id = R.color.orange_700)
            )

        )
        if (emailErrorState.value) {
            Text(text = "Required", fontFamily = fonts, color = Color.Red)
        }
        Spacer(modifier = Modifier.size(16.dp))

        val passwordVisibility = remember { mutableStateOf(true) }
        OutlinedTextField(
            value = password.value,
            shape = RoundedCornerShape(50.dp),
            onValueChange = {
                if (passwordErrorState.value) {
                    passwordErrorState.value = false
                }
                password.value = it
            },
            modifier = Modifier.fillMaxWidth(),
            label = { Text("Password", fontFamily = fonts) },
            isError = passwordErrorState.value,
            trailingIcon = {
                IconButton(onClick = {
                    passwordVisibility.value = !passwordVisibility.value
                }) {
                    Icon(
                        imageVector = if (passwordVisibility.value) Icons.Default.VisibilityOff else Icons.Default.Visibility,
                        contentDescription = "visibility",
                        tint = Color.Red
                    )
                }
            },
            visualTransformation = if (passwordVisibility.value) PasswordVisualTransformation() else VisualTransformation.None,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = colorResource(id = R.color.orange_700),
                focusedLabelColor = colorResource(id = R.color.orange_700)
            )

        )
        if (passwordErrorState.value) {
            Text(text = "Required", fontFamily = fonts, color = Color.Red)
        }
        Spacer(Modifier.size(16.dp))

        val cPasswordVisibility = remember { mutableStateOf(true) }

        OutlinedTextField(
            value = confirmPassword.value,
            shape = RoundedCornerShape(50.dp),
            onValueChange = {
                if (confirmPasswordErrorState.value) {
                    confirmPasswordErrorState.value = false
                }
                confirmPassword.value = it
            },
            label = { Text("Confirm Password", fontFamily = fonts) },
            modifier = Modifier.fillMaxWidth(),
            isError = confirmPasswordErrorState.value,
            trailingIcon = {
                IconButton(onClick = {
                    cPasswordVisibility.value = !cPasswordVisibility.value
                }) {
                    Icon(
                        imageVector = if (cPasswordVisibility.value) Icons.Default.VisibilityOff else Icons.Default.Visibility,
                        contentDescription = "visibility",
                        tint = Color.Red
                    )
                }
            },
            visualTransformation = if (cPasswordVisibility.value) PasswordVisualTransformation() else VisualTransformation.None,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                focusedBorderColor = colorResource(id = R.color.orange_700),
                focusedLabelColor = colorResource(id = R.color.orange_700)
            )

        )
        if (confirmPasswordErrorState.value) {
            val msg = when {
                confirmPassword.value.text.isEmpty() -> {
                    "Required"
                }
                confirmPassword.value.text != password.value.text -> {
                    "Password not matching"
                }
                else -> {
                    ""
                }
            }
            Text(text = msg, color = Color.Red)
        }
        Spacer(Modifier.size(16.dp))
        Button(modifier = Modifier
            .padding(top = 10.dp)
            .size(120.dp, 50.dp),
            shape = RoundedCornerShape(50.dp),
            colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.orange_700)),
            onClick = {
                when {
                    email.value.text.isEmpty() -> {
                        emailErrorState.value = true
                    }
                    password.value.text.isEmpty() -> {
                        passwordErrorState.value = true
                    }
                    confirmPassword.value.text.isEmpty() -> {
                        confirmPasswordErrorState.value = true
                    }
                    confirmPassword.value.text != password.value.text -> {
                        confirmPasswordErrorState.value = true
                    }
                    else -> {
                        Toast.makeText(
                            context,
                            "Registered successfully",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }) {
            Text("Sign Up", fontFamily = fonts, fontSize = 15.sp)
        }
        Spacer(modifier = Modifier.padding(top = 30.dp))
        TextButton(
            shape = RoundedCornerShape(50.dp),
            onClick = {
                navController.navigate("login") {
                    popUpTo(navController.graph.startDestinationId)
                    launchSingleTop = true
                }
            }
        ) {
            Text(
                text = "Already Registered? Sign In Here!",
                fontFamily = fonts,
                fontSize = 15.sp,
            )
        }
        Spacer(modifier = Modifier.padding(top = 100.dp))
    }


}
