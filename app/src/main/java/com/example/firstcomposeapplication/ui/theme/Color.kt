package com.example.firstcomposeapplication.ui.theme

import androidx.compose.ui.graphics.Color

val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)

val primaryColor = Color(255, 182, 26)