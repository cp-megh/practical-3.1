package com.example.firstcomposeapplication.model

data class Dogs(

    val id: Int,
    val image: Int,
    val name: String,
    val type: String,
    val colour: String,
    val gender: String,
    val age: Int,
)


