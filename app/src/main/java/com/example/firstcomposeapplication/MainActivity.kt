package com.example.firstcomposeapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.firstcomposeapplication.composables.HomeContent
import com.example.firstcomposeapplication.composables.LoginView
import com.example.firstcomposeapplication.composables.RegistrationView
import com.example.firstcomposeapplication.ui.theme.FirstComposeApplicationTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FirstComposeApplicationTheme {
                MaterialTheme {
                    Surface(color = MaterialTheme.colors.background) {
                        LoginAndRegistration()
                    }
                }
            }
        }
    }
}


val fonts = FontFamily(
    Font(R.font.bol_sans_bold, weight = FontWeight.Bold),
    Font(R.font.bol_sans_bold_italic, weight = FontWeight.Bold, FontStyle.Italic),
    Font(R.font.bol_sans_italic, weight = FontWeight.Normal, FontStyle.Italic),
    Font(R.font.bol_sans_regular)
)

@Composable
fun LoginAndRegistration() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "login", builder = {
        composable("login", content = { LoginView(navController = navController) })
        composable("registration", content = { RegistrationView(navController = navController) })
        composable("home", content = { HomeContent(navController = navController) })
    })
}

