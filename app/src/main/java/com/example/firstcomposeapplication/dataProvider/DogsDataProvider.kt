package com.example.firstcomposeapplication.dataProvider

import com.example.firstcomposeapplication.R
import com.example.firstcomposeapplication.model.Dogs

object DogsDataProvider {
    val dogList= listOf(
        Dogs(0, R.drawable.dog1,"Carmen","Barbet","Black","Male",1),
        Dogs(1,R.drawable.dog2,"Lola","Terrier","White","Male",2),
        Dogs(2,R.drawable.dog3,"Coco","Boxer","Red","Female",1),
        Dogs(3,R.drawable.dog4,"Wand","Pug","Black","Female",2),
        Dogs(4,R.drawable.dog5,"Coco","Boxer","Black","Male",1),
        Dogs(5,R.drawable.dog6,"Carmen","Terrier","White","Female",1),
        Dogs(6, R.drawable.dog1,"Carmen","Barbet","Black","Male",1),
        Dogs(7,R.drawable.dog2,"Lola","Terrier","White","Male",2),
        Dogs(8,R.drawable.dog3,"Coco","Boxer","Red","Female",1),
        Dogs(9,R.drawable.dog4,"Wand","Pug","Black","Female",2 ),
        Dogs(10,R.drawable.dog5,"Coco","Boxer","Black","Male",1),
        Dogs(11,R.drawable.dog6,"Carmen","Terrier","White","Female",1)
    )
}