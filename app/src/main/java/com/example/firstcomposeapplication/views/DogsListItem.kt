package com.example.firstcomposeapplication

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.firstcomposeapplication.model.Dogs

@Composable
fun DogsListItem(dogs: Dogs) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth()
            .clickable { },
        elevation = 2.dp,
        backgroundColor = Color.White,
        shape = RoundedCornerShape(corner = CornerSize(20.dp))
    ) {

        Row(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(start = 10.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                DogImage(dogs = dogs)
            }

            Column(
                modifier = Modifier
                    .padding(start = 5.dp, end = 10.dp)
                    .weight(2f)
                    .align(Alignment.Top),

                ) {
                Row {
                    Text(text = dogs.name, style = typography.h6, fontFamily = fonts)
                }
                Row {
                    TypeImage(dogs = dogs)
                }
                Row(modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center) {
                    Card(
                        modifier = Modifier
                            .padding(horizontal = 5.dp, vertical = 8.dp)
                            .weight(1f),
                        elevation = 2.dp,
                        backgroundColor = Color.Red,
                        shape = RoundedCornerShape(corner = CornerSize(5.dp))
                    ) {
                        Text(
                            text = dogs.colour,
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontFamily = fonts
                        )
                    }
                    Card(
                        modifier = Modifier
                            .padding(horizontal = 8.dp, vertical = 8.dp)
                            .weight(1f),
                        elevation = 2.dp,
                        backgroundColor = Color(60, 251, 213),
                        shape = RoundedCornerShape(corner = CornerSize(5.dp))
                    ) {
                        Text(
                            text = dogs.gender,
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontFamily = fonts
                        )
                    }
                    Card(
                        modifier = Modifier
                            .padding(horizontal = 8.dp, vertical = 8.dp)
                            .weight(1f),
                        elevation = 2.dp,
                        backgroundColor = Color(248, 212, 33, 255),
                        shape = RoundedCornerShape(corner = CornerSize(5.dp))
                    ) {
                        Text(
                            text = dogs.age.toString() + " year",
                            textAlign = TextAlign.Center,
                            color = Color.White,
                            fontFamily = fonts
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun DogImage(dogs: Dogs) {
    Image(
        painter = painterResource(id = dogs.image),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .padding(8.dp)
            .size(80.dp)
            .clip(CircleShape)
    )
}

@Composable
private fun TypeImage(dogs: Dogs) {
    Image(
        painter = painterResource(id = R.drawable.type),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .padding(0.dp)
            .size(25.dp)
            .clip(CircleShape)
    )
    Text(
        text = dogs.type,
        fontFamily = fonts,
        color = Color.Gray,
        modifier = Modifier.padding(top = 2.dp, start = 5.dp)
    )
}


