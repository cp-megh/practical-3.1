package com.example.firstcomposeapplication.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.firstcomposeapplication.fonts
import com.example.firstcomposeapplication.model.Dogs

@Composable
fun DogsChipsItems(dogs: Dogs) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .size(100.dp,40.dp)
            .clickable {  },
        elevation = 2.dp,
        backgroundColor = Color.White,
        shape = RoundedCornerShape(corner = CornerSize(20.dp))
    ) {

        Row {
                DogImage(dogs = dogs)
                Text(text = dogs.type,
                    fontFamily = fonts,
                    color = Color.Gray,
                    textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = 8.dp))

        }
    }
}

@Composable
private fun DogImage(dogs:Dogs){
    Image(
        painter = painterResource(id = dogs.image),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .padding(8.dp)
            .size(25.dp)
            .clip(CircleShape)
    )

}